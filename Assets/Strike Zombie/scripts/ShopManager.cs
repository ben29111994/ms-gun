﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
	public DataGame dataGame;
	public UIManager uiManager;
	public RectTransform listItem;
	public Item itemPrefab;
	public Button buttonSelect;
	public float deltaItem;
	public PlayerRender playerRender;

	void Awake ()
	{
		dataGame.spritePlayer [0].purchase = true;
		SetShop ();
		gameObject.SetActive (false);
	}

	void OnEnable ()
	{
		dataGame.playerSelect = dataGame.playerActive;
	}

	[ContextMenu ("Creat Shop")]
	public void SetShop ()
	{
		//playerRender1.SetSprite (dataGame.playerActive);
		listItem.sizeDelta = new Vector2 (deltaItem * dataGame.spritePlayer.Length, listItem.sizeDelta.y);
		dataGame.playerSelect = dataGame.playerActive;
		for (int i = 0; i < dataGame.spritePlayer.Length; i++) {
			GameObject item = (GameObject)Instantiate (itemPrefab.gameObject);
			item.transform.SetParent (listItem.transform, false);
			RectTransform rectTran = item.GetComponent<RectTransform> ();
			rectTran.localPosition = new Vector3 ((i + 0.5f) * deltaItem, rectTran.localPosition.y, rectTran.localPosition.z);
			Item itemComponent = item.GetComponent<Item> ();
			itemComponent.playerShop = playerRender;
			itemComponent.shopManager = this;
			itemComponent.SetItem (dataGame);
		}
	}

	void ButtonPurchase ()
	{
		if (dataGame.coin >= dataGame.spritePlayer [dataGame.playerSelect].coin) {   
			dataGame.spritePlayer [dataGame.playerSelect].purchase = true;
			uiManager.AddCoin (-dataGame.spritePlayer [dataGame.playerSelect].coin);
			listItem.transform.GetChild (dataGame.playerSelect).GetComponent<Item> ().CheckPurchase ();
			CheckSelect ();
		} else {
			uiManager.PingCoin ();
		}
	}

	public void ButtonSelect ()
	{
		if (dataGame.spritePlayer [dataGame.playerSelect].purchase) {
			dataGame.playerActive = dataGame.playerSelect;
			playerRender.SetSpritePlayer (dataGame.playerActive);
			uiManager.ButtonHideShop ();
		} else {
			ButtonPurchase ();
		}
	}

	public void CheckSelect ()
	{
		if (dataGame.spritePlayer [dataGame.playerSelect].purchase) {
			buttonSelect.transform.GetChild (0).GetComponent<Text> ().text = "Select";
		} else {
			if (dataGame.coin >= dataGame.spritePlayer [dataGame.playerSelect].coin) { 
			} else {
			}
			buttonSelect.transform.GetChild (0).GetComponent<Text> ().text = "Buy";
		}
	}
}
