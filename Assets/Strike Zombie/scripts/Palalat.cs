﻿using UnityEngine;
using System.Collections;
using System;

public class Palalat : MonoBehaviour
{
	public enum AutoScale
	{
		nonScale,
		scaleX,
		scaleY,
		scaleXY,
		scaleXYfix
	}

	public AutoScale autoScale;
	public float speed;
	public float scaleSpeedCamera;
	public CameraMove cam;
	public Vector2 delta;
	public Vector2 deltaRemove;
	public bool staticCamera = false;
	public bool autoRemoveCamera = false;

	SpriteRenderer render;
	Vector3 currenCamera;
	Transform _transform;
	Vector3 beginTran;

	void SetData ()
	{
		float scaleX = cam.widthScene * 2 / render.sprite.bounds.size.x;
		float scaleY = Camera.main.orthographicSize * 2 / render.sprite.bounds.size.y;

		if (autoRemoveCamera) {
			deltaRemove = new Vector2 (render.sprite.bounds.size.x * _transform.localScale.x / 2 + Camera.main.orthographicSize * (float)Screen.width / (float)Screen.height, deltaRemove.y);
		}

		if (autoScale == AutoScale.nonScale) {
			return;
		} else if (autoScale == AutoScale.scaleX) {
			_transform.localScale = new Vector3 (_transform.localScale.x * scaleX, _transform.localScale.y, _transform.localScale.z);
		} else if (autoScale == AutoScale.scaleY) {
			_transform.localScale = new Vector3 (_transform.localScale.x, _transform.localScale.y * scaleY, _transform.localScale.z);
		} else if (autoScale == AutoScale.scaleXY) {
			_transform.localScale = new Vector3 (_transform.localScale.x * scaleX, _transform.localScale.y * scaleY, _transform.localScale.z);
		} else if (autoScale == AutoScale.scaleXYfix) {
			_transform.localScale = new Vector3 (_transform.localScale.x * Math.Max (scaleX, scaleY), _transform.localScale.y * Math.Max (scaleX, scaleY), _transform.localScale.z);
		} 
	}

	void OnEnable ()
	{
		_transform.position = beginTran;
	}

	void Awake ()
	{
		_transform = GetComponent<Transform> ();
		render = GetComponent<SpriteRenderer> ();
		cam = Camera.main.gameObject.GetComponent<CameraMove> ();
		currenCamera = cam.transform.position;
		SetData ();
		beginTran = _transform.position;
	}

	void LateUpdate ()
	{
		if (speed != 0) {
			Move (speed);
		}
		if (cam.move) {
			if (staticCamera) {
				_transform.position = new Vector3 (cam.transform.position.x, _transform.position.y, _transform.position.z);
			} else {
				Move (scaleSpeedCamera);
			}
		}
		if (cam.transform.position.x - _transform.position.x > deltaRemove.x) {
			ReMove ();
		}	
	}

	void Move (float speedMove)
	{
		_transform.position = new Vector3 (_transform.position.x + speedMove * Time.deltaTime,
			_transform.position.y, _transform.position.z);
	}

	void ReMove ()
	{
		_transform.localPosition = new Vector3 (_transform.position.x + delta.x, _transform.position.y + delta.y, _transform.position.z);
	}
}
