﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class AudioData
{
	public AudioClip backGround;
	public AudioClip playerRun;
	public AudioClip playerDeath;
	public AudioClip arrowShot;
	public AudioClip kill;
	public AudioClip headShot;
	public AudioClip ultraKill;
	public AudioClip spear;
	public AudioClip coin;
}

[System.Serializable]
public class SpriteObjectPlayer
{
	public bool buy = false;
	public bool purchase
	{
		get{ return PlayerPrefs.HasKey (name);}
		set{ if (value) {
				PlayerPrefs.SetInt (name, 1);
		} else {
			PlayerPrefs.DeleteKey (name);
		}
		buy = value;
	}
	}
	public string name;
	public int coin;
	public Sprite avatar;
	public PlayerControl player;
	public GameObject playerShop;
}

[System.Serializable]
public class TipData
{
	public string tipBegin;
	public string[] ListTipRandom;
}

[System.Serializable]
public class ColorUI
{
	public Color select;
	public Color unSelect;
}

[System.Serializable]
public class DataGame : ScriptableObject
{
	public bool deleteAllData = false;
	public int score;
	public int coins;
	public int coin
	{
		get{return (PlayerPrefs.HasKey ("coin"))?PlayerPrefs.GetInt ("coin"):0;}
		set{ PlayerPrefs.SetInt ("coin", value);
			coins = value;
		}
	}
	public float volum = 1;
	public int playerActiv;

	public int playerActive
	{
		get{ return (PlayerPrefs.HasKey ("playerActive"))?PlayerPrefs.GetInt ("playerActive"):0;}
		set{ PlayerPrefs.SetInt ("playerActive", value);
			playerActiv = value;
		}
	}
	public int playerSelect = 0;
	public SpriteObjectPlayer[] spritePlayer;
	public AudioData audioData;
	public TipData tipData;
	public ColorUI colorUI;
}
