﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class GroundMove : MonoBehaviour
{
	float delta = 10.15f;
	Vector3 _transform;
	CameraMove cam;
	SpriteRenderer render;

	void Awake ()
	{
		cam = Camera.main.GetComponent<CameraMove> ();
		render = GetComponent<SpriteRenderer> ();
		_transform = transform.position;
	}

	void OnEnable ()
	{
		transform.position = _transform;
		delta = render.sprite.bounds.size.x * transform.parent.childCount - 0.2f;
	}

	void Update ()
	{
		if (cam.transform.position.x - transform.position.x > render.sprite.bounds.size.x + cam.widthScene) {
			transform.localPosition = new Vector3 (transform.localPosition.x + delta,
				transform.localPosition.y,
				transform.localPosition.z);
		}
	}
}