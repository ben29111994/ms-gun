﻿using UnityEngine;
using System.Collections;

public class ArcheryControl : MonoBehaviour
{
	public float speed;
	public GameObject handRight;
	public GameObject archery;
	public ArrowControl arrowControl;
	public float maxAngle = 170;
	public float minAngle = 0f;
	[HideInInspector]
	public bool turn = false;
	Vector3 euluArchery;
	Vector3 posiArchery;
	Vector3 description;
	GameObject effect;
	bool isMoveDown;
	void OnEnable ()
	{
		euluArchery = transform.localEulerAngles;
		posiArchery = transform.localPosition;
		if(effect == null)
		{
			effect = Camera.main.transform.GetChild (0).gameObject;
		}
	}

	
	void Update ()
	{
		if (turn) {
	//		transform.RotateAround (handRight.transform.position, Vector3.forward, speed * Time.deltaTime);
//			if (transform.eulerAngles.z >= 170) {
//				isMoveDown = true;
//			} else if (transform.eulerAngles.z <= 90) {
//				isMoveDown = false;
//			}
//			if (isMoveDown) {
//				transform.RotateAround (handRight.transform.position, Vector3.forward, speed * Time.deltaTime);
//			} else {
//				transform.RotateAround (handRight.transform.position, Vector3.forward, speed * Time.deltaTime);
//			}
			if (PlayerControl.instance.facingLeft) {
				if (transform.eulerAngles.z >= 170) {
					isMoveDown = true;
				} else if (transform.eulerAngles.z <= 90) {
					isMoveDown = false;
				}
				if (isMoveDown) {
					transform.RotateAround (handRight.transform.position, Vector3.forward, -speed * Time.deltaTime);
				} else {
					transform.RotateAround (handRight.transform.position, Vector3.forward, speed * Time.deltaTime);
				}
			} else {
				if (transform.eulerAngles.z >= 280) {
					isMoveDown = false;
				} else if (transform.eulerAngles.z <= 190) {
					isMoveDown = true;
				}
				if (isMoveDown) {
					transform.RotateAround (handRight.transform.position, Vector3.forward, speed * Time.deltaTime);
				} else {
					transform.RotateAround (handRight.transform.position, Vector3.forward, -speed * Time.deltaTime);
				}
			}
			//Debug.Log (transform.eulerAngles.z);
			effect.transform.position = new Vector3 (arrowControl.transform.position.x, arrowControl.transform.position.y, arrowControl.transform.position.z);
			effect.transform.GetChild (0).GetChild (0).eulerAngles = new Vector3(0,0,transform.eulerAngles.z - 180);
		}
		handRight.transform.eulerAngles = transform.eulerAngles;
	}


	public void TurnArchery ()
	{
		effect.SetActive (true);
		turn = true;
		arrowControl.gameObject.SetActive (true);
		//effect.transform.position = arrowControl.transform.position;
	}

	public void Shot ()
	{
		effect.SetActive (false);
		turn = false;
		arrowControl.Shoot ();
	}

	public void SetArchery ()
	{
		transform.localEulerAngles = euluArchery;
		transform.localPosition = posiArchery;
		arrowControl.SetArrow ();
	}
}
