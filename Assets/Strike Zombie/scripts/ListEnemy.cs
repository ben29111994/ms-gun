﻿using UnityEngine;
using System.Collections;

public class ListEnemy : MonoBehaviour
{
	public EnemyControl currenEnemy;
	EnemyControl[] listEnemy;
	// Use this for initialization
	void Start ()
	{
		listEnemy = new EnemyControl[transform.childCount];
		for (int i = 0; i < transform.childCount; i++) {
			listEnemy [i] = transform.GetChild (i).GetComponent<EnemyControl> ();
			listEnemy [i].gameObject.SetActive (false);
		}
	}
	
	// Update is called once per frame
	public void ResetEnemy ()
	{
		for (int i = 0; i < listEnemy.Length; i++) {
			listEnemy [i].SetEnemy ();
			listEnemy [i].gameObject.SetActive (false);
		}
	}

	public void GetEnemy ()
	{
		while (true) {
			int i = Random.Range (0, listEnemy.Length);
			if (!listEnemy [i].gameObject.activeSelf) {
				currenEnemy = listEnemy [i];
				currenEnemy.gameObject.SetActive (true);
				return;
			}
		}
	}
}
