﻿using UnityEngine;
using System.Collections;

public class ScaleScene : MonoBehaviour {

    SpriteRenderer sprite;
	// Use this for initialization

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

	void Start () {
        transform.localScale = new Vector3((float)Screen.width / (float)Screen.height * Camera.main.orthographicSize * 2.02f / sprite.bounds.size.x , transform.localScale.y, transform.localScale.z);
    }
	
}
