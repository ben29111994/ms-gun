﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tips : MonoBehaviour
{
	public DataGame dataGame;
	public GameObject tipPanel;

	Text textTip;

	void Awake ()
	{
		textTip = tipPanel.transform.GetChild (0).gameObject.GetComponent<Text> ();
	}

	public void StartGame ()
	{
		tipPanel.SetActive (true);
		textTip.text = "TIPS: " + dataGame.tipData.tipBegin;
	}

	public void TipRandom ()
	{
		tipPanel.SetActive (true);
		textTip.text = "TIPS: " + dataGame.tipData.ListTipRandom [Random.Range (0, dataGame.tipData.ListTipRandom.Length)];
	}

	public void DestroyTips ()
	{
		tipPanel.SetActive (false);
	}
}
