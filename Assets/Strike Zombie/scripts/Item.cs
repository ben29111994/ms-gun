﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
	public Image avata;
	public DataGame dataGame;
	public PlayerRender playerShop;
	public ShopManager shopManager;
	public GameObject purchase;
	public Text coin;
	public Text name;

	public void SetItem (DataGame data)
	{
		dataGame = data;
		avata.sprite = data.spritePlayer [gameObject.transform.GetSiblingIndex ()].avatar;
		name.text = data.spritePlayer [gameObject.transform.GetSiblingIndex ()].name;
//		RectTransform _rectTranAvata = avata.GetComponent<RectTransform> ();
//		_rectTranAvata.sizeDelta = new Vector2 (_rectTranAvata.sizeDelta.x, 
//			_rectTranAvata.sizeDelta.x * (float)data.spritePlayer [gameObject.transform.GetSiblingIndex ()].avatar.bounds.size.y / (float)data.spritePlayer [gameObject.transform.GetSiblingIndex ()].avatar.bounds.size.x);
		CheckPurchase ();
		if(dataGame.playerActive == gameObject.transform.GetSiblingIndex ())
		{
			SelectItem ();
		}
	}

	void OnEnable()
	{
		if(dataGame != null && dataGame.playerActive == gameObject.transform.GetSiblingIndex ())
		{
			SelectItem ();
		}
	}

	public void SelectItem ()
	{
		//shopManager.ActiveItem (dataGame.playerSelect, false);
		dataGame.playerSelect = gameObject.transform.GetSiblingIndex ();
		playerShop.SetSpriteShop (dataGame.playerSelect);
		shopManager.CheckSelect ();
		//shopManager.ActiveItem (dataGame.playerSelect, true);

	}

	public void CheckPurchase ()
	{
		if (!dataGame.spritePlayer [transform.GetSiblingIndex ()].purchase) {
			purchase.SetActive (true);
			coin.text = dataGame.spritePlayer [transform.GetSiblingIndex ()].coin.ToString ();
		} else {
			purchase.SetActive (false);
		}
	}

	void Update ()
	{
		if (dataGame.playerSelect == transform.GetSiblingIndex ()) {
			GetComponent<Animator> ().SetTrigger ("Highlighted");
		} else {
			GetComponent<Animator> ().SetTrigger ("Normal");
		}
	}
}
