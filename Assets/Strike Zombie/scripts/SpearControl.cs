﻿using UnityEngine;
using System.Collections;

public class SpearControl : MonoBehaviour
{
	public DataGame dataGame;
	public float speed = 12;
	public PlayerRender playRender;
	PlayerControl playerControl
	{
		get{return playRender.player;}
	}
	public GameObject enemy;
	public UIManager managerUI;
	public ParticleSystem particle;
	public Transform hand;
	public SpriteRenderer render;
	BoxCollider2D colliderSpear;
	Vector3 tranStart;
	Vector3 euluStart;
	Vector3 scaleStart;
	Vector3 handStartRotate;
	Transform _transform;
	bool shoot = false;
	Vector2 description;
	Animation anima;
	AudioSource audio;

	void Awake ()
	{
		audio = GetComponent<AudioSource> ();
		_transform = GetComponent<Transform> ();
		colliderSpear = GetComponent<BoxCollider2D> ();
		anima = GetComponent<Animation> ();
		audio.clip = dataGame.audioData.spear;
		tranStart = _transform.localPosition;
		euluStart = _transform.localEulerAngles;
		scaleStart = _transform.localScale;
		handStartRotate = hand.eulerAngles;
	}

	IEnumerator MoveSpear ()
	{
		yield return new WaitForEndOfFrame ();
		_transform.position = new Vector3 (_transform.position.x + description.x * speed * Time.deltaTime, _transform.position.y + description.y * speed * Time.deltaTime, _transform.position.z);
		if (shoot) {
			StartCoroutine (MoveSpear ());
		}
	}

	public void Shoot () // goi 1 lan
	{
		render.enabled = true;
		particle.Play ();
		audio.Play ();
		shoot = true;
		description = new Vector2 (playerControl.gameObject.transform.position.x - _transform.position.x,
			playerControl.gameObject.transform.position.y + 0.5f - _transform.position.y).normalized;
		hand.rotation = Quaternion.FromToRotation (hand.right, description);
		_transform.SetParent (null);
		StartCoroutine (MoveSpear ());

		colliderSpear.enabled = true;
	}

	public void SetSpear ()
	{
		hand.eulerAngles = handStartRotate;
		particle.Stop ();
		_transform.SetParent (hand);
		_transform.localEulerAngles = euluStart;
		_transform.localPosition = tranStart;
		_transform.localScale = scaleStart;
		colliderSpear.enabled = false;
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.gameObject.tag == "Player") {
			colliderSpear.enabled = false;
			render.enabled = false;
			playerControl.DiePlayer ();
			Invoke ("GameOver", 0.7f);
			shoot = false;
			anima.Play ();
			particle.Stop ();
			if(audio.isPlaying)
			{
				audio.Stop ();
			}
			audio.clip = dataGame.audioData.kill;
			audio.Play ();
		}
	}

	void DelayParticle ()
	{
        
	}

	void GameOver ()
	{
		managerUI.GameOver ();
	}
}