﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class ZoomCamera
{
	public enum CameraZoomTyoe
	{
		center,
		pivot
	}

	public CameraZoomTyoe zoom;
	[Range (0, 1)]
	public float speedLerpZoom = 0.2f;
	[Range (0, 1)]
	public float speedLerpUnZoom = 0.2f;
	public float zoomMax = 3;
	[Range (0, 1)]
	public float timeScaleZoom = 0.15f;
	public float chromaticEffect = 5;
}

[System.Serializable]
public class RandomBackGround
{
	public GameObject backGround;
	public GameObject palalat;

	public void Active (bool active)
	{
		backGround.SetActive (active);
		palalat.SetActive (active);
	}
}

public class CameraMove : MonoBehaviour
{
	public DataGame dataGame;
	public RandomBackGround[] backGround;
	public Image nameGame;
	public bool useEffectZoom = false;
	public ZoomCamera zoomCamera;
	public bool move = false;
	public bool resetCam = false;
	public float speed = 7;
	public GameObject player;
	public UIManager uiManager;
	[HideInInspector]
	public float widthScene;
	Vector3 tranCamera;
	Vector3 cureCameraPosi;
	float ofset;
	float orthog;

	// Use this for initialization
	void Awake ()
	{
		GetComponent<AudioSource> ().clip = dataGame.audioData.backGround;
		orthog = Camera.main.orthographicSize;
		widthScene = (float)Screen.width / (float)Screen.height * Camera.main.orthographicSize;
		ofset = transform.position.y - player.transform.position.y;
		tranCamera = transform.position;
		SetCamera ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (transform.position.y < player.transform.position.y + ofset) {
			move = true;
			MoveCamera ();
		} else {
			move = false;
		}
		if (resetCam) {
			ResetCam ();
		}
	}

	void MoveCamera ()
	{
		transform.position = new Vector3 (transform.position.x,
			transform.position.y +speed * Time.deltaTime,
			transform.position.z);
	}

	public void SetCamera ()
	{
		transform.position = cureCameraPosi;
		Camera.main.orthographicSize = orthog;
		transform.position = tranCamera;
		int randomGround = Random.Range (0, backGround.Length);
		for (int i = 0; i < backGround.Length; i++) {
			if (i == randomGround) {
				backGround [i].Active (false);
				backGround [i].Active (true);

			} else {
				backGround [i].Active (false);
			}
		}
	}

	public void CameraZoom (Vector3 play, Vector3 enemy)
	{
		float deltaX = Mathf.Abs (play.x - enemy.x);
		if (zoomCamera.zoom == ZoomCamera.CameraZoomTyoe.center) {
			Camera.main.orthographicSize = Mathf.Lerp (Camera.main.orthographicSize, Mathf.Clamp (Mathf.Abs ((deltaX + 2.5f) / (widthScene * 2)) * Camera.main.orthographicSize, orthog / zoomCamera.zoomMax, orthog), zoomCamera.speedLerpZoom);
			transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.FromToRotation (Vector3.forward, (play + enemy) / 2 - transform.position), zoomCamera.speedLerpZoom);
		} else {			
			Camera.main.orthographicSize = Mathf.Lerp (Camera.main.orthographicSize, Mathf.Clamp (Mathf.Abs ((deltaX + 2.5f) * 2 / (widthScene * 2)) * Camera.main.orthographicSize, orthog / zoomCamera.zoomMax, orthog), zoomCamera.speedLerpZoom);
			transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.FromToRotation (Vector3.forward, play - transform.position), zoomCamera.speedLerpZoom);
		}
		Time.timeScale = Mathf.Lerp (Time.timeScale, zoomCamera.timeScaleZoom, zoomCamera.speedLerpZoom);
	}

	public void ResetCam ()                // dua camera ve trang thai cu
	{
		Camera.main.orthographicSize = Mathf.Lerp (Camera.main.orthographicSize, orthog, zoomCamera.speedLerpUnZoom);
		transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.FromToRotation (transform.forward, Vector3.forward), zoomCamera.speedLerpUnZoom);
		if (Camera.main.orthographicSize - orthog >= -0.1f) {
			Camera.main.orthographicSize = orthog;
			resetCam = false;
		}
	}

	public void SetCam ()
	{
		cureCameraPosi = transform.position;
	}
}
