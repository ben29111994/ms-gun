﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerControl : MonoBehaviour
{
	public static PlayerControl instance;
	public float movementSpeed;
	public DataGame dataGame;
	public TypePlayer type;
	public bool buttonClick = false;
	public float speed = 8;
	public Vector2 deltaRandom = new Vector2 (9, 13);
	// Random khoang cach den diem tiep theo
	public Vector3 positionEnd;
	public CameraMove cameraMove;
	public ArcheryControl archeryControl;
	public UIManager uiManager;
	public bool setPlay = true;
	public GameObject ground;
	public ListEnemy listEnemy;
	public ParticleSystem particleDeath;
	public Vector3 tranStart;
	Animator anima;
	AudioSource audio;
	Rigidbody2D rigid;
	Vector3 currntStarePosition;
	public bool isGrounded;
	public LayerMask layer;
	public bool facingLeft;
	public GameObject anchery;
	public Transform arrowControl;
	public enum TypePlayer
	{
		run,
		ide,
		die,
		turn,
		shot
	}
	
	// Use this for initialization
	void OnEnable ()
	{
		audio = GetComponent<AudioSource> ();
		anima = GetComponent<Animator> ();
		rigid = GetComponent <Rigidbody2D> ();


	}

	void Awake()
	{
		anchery = GameObject.Find ("Effect");
		instance = this;
		tranStart = transform.localPosition;
		facingLeft = true;
	}
		
	
	// Update is called once per frame
	void Update ()
	{
		RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector2.down, 1,layer);
		if (hit != null) {
			isGrounded = true;
		} else {
			isGrounded = false;
		}


        EventSystem eventSystem = EventSystem.current;
        if (eventSystem.currentSelectedGameObject != null)
        {
            return;
        }

        if (type == TypePlayer.die && rigid.velocity.y < -50)
		{
			rigid.isKinematic = true;
		}
		if (type == TypePlayer.run) {
			RunPlayer ();
		}

		if (!cameraMove.move) {
			if (type == TypePlayer.ide) {
				if (Input.GetMouseButtonDown (0) && !WasAButton ()) {
				//	PlayerTurn ();
				}
			} else if (type == TypePlayer.turn) {
				if (Input.GetMouseButtonUp (0) && !WasAButton ()) {
					Debug.LogError (" on click shoot ");
					PlayerShoot ();
				}
			}
		}
	}

	private bool WasAButton()
	{
		UnityEngine.EventSystems.EventSystem ct
		= UnityEngine.EventSystems.EventSystem.current;

		if (! ct.IsPointerOverGameObject() ) return false;
		if (! ct.currentSelectedGameObject ) return false;
		if (ct.currentSelectedGameObject.GetComponent<Button>() == null )
			return false;

        if (ct.currentSelectedGameObject != null)
            return true;

		return true;
	}

	void RunPlayer ()
	{
//		transform.position = new Vector3 (transform.position.x + speed * Time.deltaTime,
//			transform.position.y,
//			transform.position.z);
		if (isGrounded)
			rigid.AddForce (new Vector2 (movementSpeed, 0));
		else {
			rigid.velocity = Vector2.zero;
		}
		rigid.velocity = new Vector2 (Mathf.Clamp (rigid.velocity.x,0, 20), rigid.velocity.y);
//		if (transform.position.x >= positionEnd.x) {
//			transform.position = positionEnd;
//			IdePlayer ();
//		}
	}

	public void PlayerShoot ()
	{
		if (type == TypePlayer.turn) {
			type = TypePlayer.shot;
			anima.SetTrigger ("shot");
			archeryControl.Shot ();
			currntStarePosition = transform.position;
			Debug.LogError (" shoot ");
		}
	}

	public void SaveState()
	{
		transform.position = positionEnd;
		rigid.isKinematic = true;
		transform.GetChild (0).localEulerAngles = Vector3.zero;
        rigid.velocity = Vector2.zero;
		anima.SetTrigger ("resume");
		IdePlayer ();
		archeryControl.SetArchery ();
		listEnemy.currenEnemy.SetEnemy ();
	}

	public void IdePlayer ()
	{
		Debug.Log (" idle player ");
		if (audio.isPlaying) {
			audio.Stop ();
		}
		type = TypePlayer.ide;
		anima.SetBool ("run", false);
		anima.SetBool ("turn", false);
		transform.position = positionEnd;
		StartCoroutine( PlayerTurn ());
	}

	public void DiePlayer ()
	{
		audio.clip = dataGame.audioData.playerDeath;
		audio.Play ();
		type = TypePlayer.die;
		rigid.isKinematic = false;
		rigid.AddForce (new Vector2(Random.Range (-100,0), Random.Range (300,900)));
		anima.SetTrigger ("death");
		if (particleDeath.isPlaying) {
			particleDeath.Stop ();
		}
		particleDeath.transform.position = transform.position;
		particleDeath.Play ();
	}

	public IEnumerator PlayerTurn ()
	{	Debug.Log (" turn ");
		yield return new WaitForSeconds (.3f);
		if (type == TypePlayer.ide  && !cameraMove.move) {
			Debug.Log (" turn ");
			type = TypePlayer.turn;
			anima.SetBool ("turn", true);
			archeryControl.TurnArchery ();

		}
	}

	public void SetRun ()
	{
		audio.clip = dataGame.audioData.playerRun;
		audio.Play ();
		type = TypePlayer.run;
		anima.SetBool ("run", true);
//		positionEnd = new Vector3 (transform.position.x + (int)Random.Range (deltaRandom.x, deltaRandom.y),
//			transform.position.y,
//			transform.position.z);
		listEnemy.GetEnemy ();
		listEnemy.currenEnemy.SetRandomEnemy ();
		archeryControl.SetArchery ();

	}

	public void SetPlayer ()
	{
		transform.localPosition = tranStart;
		rigid.isKinematic = true;
        rigid.velocity = Vector2.zero;
		transform.GetChild (0).localEulerAngles = Vector3.zero;
		archeryControl.arrowControl.fire = 0;
		SetRun ();
	}

	IEnumerator Death ()
	{
		yield return new WaitForEndOfFrame ();
		if (type == TypePlayer.die) {
			transform.GetChild (0).localEulerAngles = Vector3.MoveTowards (transform.GetChild (0).localEulerAngles, new Vector3 (0, 0, 90), Time.deltaTime * 360);
			if (transform.eulerAngles.z != 90) {
				StartCoroutine (Death ());
			}
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "EnemySight") {
			positionEnd=transform.position;
			IdePlayer ();
			rigid.velocity = Vector2.zero;

			Vector3 scale = transform.localScale;
			scale.x *= -1f;
			transform.localScale = scale;

			Vector3 scaleAnchery = anchery.transform.localScale;
			scaleAnchery.x *= -1f;
			anchery.transform.localScale = scaleAnchery;


			Debug.Log (facingLeft);
			facingLeft = !facingLeft;
			Debug.Log (facingLeft);
		
		}
	}
		
}
