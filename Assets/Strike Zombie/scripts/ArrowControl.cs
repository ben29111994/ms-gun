﻿using UnityEngine;
using System.Collections;

public class ArrowControl : MonoBehaviour
{
	public DataGame dataGame;
	public float speed = 8;
	public float gravity = 1;
	public int fire = 0;
	public float topZoom = 1.5f;
	public float bottonZoom = 1;
	public ArcheryControl archeryControl;
	public ListEnemy listEnemy;
	public PlayerControl playerControl;
	public UIManager managerUI;
	public ParticleSystem particle;
	public GameObject listCoin;
	public ParticleSystem particleDeath;
	public ParticleSystem particleMiss;

	Coin[] ListCoin = new Coin[2];
	EnemyControl enemyControl;
	EdgeCollider2D colliderArrow;
	Transform _transform;
	CameraMove cam;
	Vector3 tranArrow;
	Vector3 euluArrow;
	Vector3 scaleArrow;
	SpriteRenderer spriteArrow;
	float rad;
	bool zoom = false;
	Vector2 speedArrow;
	Vector3 currenArrow;
	bool move;
	float positionZoom;
	Animation anima;
	bool miss = false;
	bool killed = false;
	public AudioSource audioSkill;
	public AudioSource audioShot;

	// Use this for initialization
	public void SetStart ()
	{
		if(listCoin == null)
		{
			listCoin = GameObject.FindGameObjectWithTag ("List Coin");
		}
		ListCoin = listCoin.GetComponentsInChildren<Coin> ();
		_transform = GetComponent<Transform> ();
		cam = Camera.main.GetComponent<CameraMove> ();
		spriteArrow = transform.GetChild (0).GetComponent<SpriteRenderer> ();
		anima = GetComponent<Animation> ();
		colliderArrow = GetComponent<EdgeCollider2D> ();
		tranArrow = transform.localPosition;
		euluArrow = transform.localEulerAngles;
		scaleArrow = transform.localScale;
		move = false;
	}

	void Start()
	{
		 	 

		SetStart ();
	}


	void Update ()
	{
		float distance = Vector2.Distance (transform.position, PlayerControl.instance.transform.position);
		if (distance > 20) {
					Miss ();

		}
		if (move) {
			Move ();
			CheckMiss ();
		}
	}

	void LateUpdate ()
	{
		if (move) {
			Zoom ();
		}
	}

	void CheckMiss ()
	{
		if (!miss && _transform.position.x - cam.transform.position.x > cam.widthScene) {
			Miss ();
			spriteArrow.enabled = false;
		}
	}

	void Move ()
	{


		_transform.position = new Vector3 (_transform.position.x + speedArrow.x * Time.deltaTime, _transform.position.y + speedArrow.y * Time.deltaTime, _transform.position.z);
		speedArrow = new Vector2 (speedArrow.x, speedArrow.y - gravity * Time.deltaTime);
		transform.eulerAngles = new Vector3 (0, 0, Mathf.Atan2 (_transform.position.y - currenArrow.y, _transform.position.x - currenArrow.x) * Mathf.Rad2Deg);
		currenArrow = _transform.position;
	}

	public void Shoot ()
	{
		particle.gameObject.SetActive (true);
		StartCoroutine (PlayAudio (audioSkill,dataGame.audioData.arrowShot));
		miss = false;
		killed = false;
		colliderArrow.enabled = true;
		currenArrow = _transform.position;
		cam.SetCam ();
		transform.SetParent (null);
		move = true;
		speed *= playerControl.transform.localScale.x;	
		rad = transform.eulerAngles.z * Mathf.Deg2Rad;
		speedArrow = new Vector2 ((speed) * Mathf.Cos (rad), (speed) * Mathf.Sin (rad));
		CheckZoom ();
	}

	void Zoom ()
	{
		if (zoom) {
			if (transform.position.x >= positionZoom) {
				cam.CameraZoom (transform.position, enemyControl.transform.position);
			}
			if (transform.position.x > enemyControl.transform.position.x) {
				cam.resetCam = true;
				zoom = false;
				Time.timeScale = 1;
			}
		}
	}

	void CheckZoom ()
	{
		if (cam.useEffectZoom && enemyControl.transform.position.x - cam.transform.position.x > cam.widthScene / 4) {
			float deltaY = GetValueY (enemyControl.transform.position.x) - enemyControl.transform.position.y;
			if (deltaY > -bottonZoom && deltaY < topZoom) {
				zoom = true;                                 // trung dich
				positionZoom = (playerControl.transform.position.x + enemyControl.transform.position.x) / 2;
			} else {
				zoom = false;
			}
		}
	}

	public void SetArrow ()
	{
		//listEnemy.GetEnemy ();
		enemyControl = listEnemy.currenEnemy;
		//enemyControl.SetRandomEnemy ();
		particle.gameObject.SetActive (false);
		spriteArrow.enabled = true;
		colliderArrow.enabled = false;
		anima.Stop ();
		transform.SetParent (archeryControl.archery.transform);
		move = false;
		transform.localEulerAngles = euluArrow ;
		transform.localPosition = tranArrow;
		transform.localScale = scaleArrow;
	}

	public float GetValueY (float x)
	{
		float deltaX = x - transform.position.x;
		return transform.position.y + deltaX * Mathf.Tan (rad) - gravity * deltaX * deltaX / 2 / speed / speed / Mathf.Cos (rad) / Mathf.Cos (rad);
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		if (!killed && (coll.gameObject.tag == "Ground" || coll.gameObject.tag == "Tower")) {
//			Miss ();
//			if(coll.gameObject.tag == "Tower")
//			{
//				coll.gameObject.transform.GetComponentInParent <Animator>().SetTrigger ("miss");
//				if (particleMiss.isPlaying) {
//					particleMiss.Stop ();
//				}
//				particleMiss.transform.position = transform.position;
//				particleMiss.Play ();
//			}
//			if (zoom) {
//				cam.resetCam = true;
//				Time.timeScale = 1;
//			}
		} else if (!miss) {
			if (coll.gameObject.tag == "HeadEnemy") {
				StartCoroutine (PlayAudio (audioShot,dataGame.audioData.kill));
				if (fire >= 3) {
					UltraKill ();
					StartCoroutine (KillStopZoom (0));
				} else {
					HeadShot ();
					StartCoroutine (KillStopZoom (0));
				}
			} else if (coll.gameObject.tag == "Enemy") {
				StartCoroutine (PlayAudio (audioShot,dataGame.audioData.kill));
				Kill ();
				StartCoroutine (KillStopZoom (0));
			}
		}

	}

	void UltraKill ()
	{
		ParticleDeath ();
		enemyControl.DieEnemy ();
		AddCoin (5);
		move = false;
		killed = true;
		fire = 0;
		StartCoroutine (PlayAudio (audioSkill,dataGame.audioData.ultraKill));
		managerUI.Shot ("ULTRA KILL +5", 5);
		spriteArrow.enabled = false;
		playerControl.SetRun ();
		particle.gameObject.SetActive (false);
	}

	void HeadShot ()
	{
		ParticleDeath ();
		enemyControl.DieEnemy ();
		AddCoin (2);
		move = false;
		killed = true;
		fire += 1;
		StartCoroutine (PlayAudio (audioSkill,dataGame.audioData.headShot));
		managerUI.Shot ("HEADSHOT +2", 2);
		spriteArrow.enabled = false;
		playerControl.SetRun ();
		particle.gameObject.SetActive (false);
	}

	void Kill ()
	{
		ParticleDeath ();
		enemyControl.DieEnemy ();
		AddCoin (1);
		move = false;
		killed = true;
		fire = 0;
		spriteArrow.enabled = false;
		managerUI.Shot ("+1", 1);
		playerControl.SetRun ();
		particle.gameObject.SetActive (false);
	}

	void Miss ()
	{
		if (!miss) {
			move = false;
			fire = 0;
			miss = true;
			enemyControl.AtTack ();
			managerUI.Shot ("MISS", 0);
			anima.Play ();
			particle.gameObject.SetActive (false);
			spriteArrow.enabled = false;
			colliderArrow.enabled = false;
		}
	}

	IEnumerator KillStopZoom (float wait)
	{
		if (cam.useEffectZoom) {
			zoom = false;
			Time.timeScale = 1;
			yield return new WaitForSeconds (wait);
			cam.resetCam = true;
		}
	}

	IEnumerator PlayAudio (AudioSource audi ,AudioClip clip)
	{
		audi.clip = clip;
		audi.Play ();
		yield return new WaitForSeconds (1.5f);
		if (audi.isPlaying) {
			audi.Stop ();
		}
	}

	void AddCoin (int count)
	{
		for (int i = 0; i < count; i++) {
			ListCoin [i].gameObject.SetActive (true);
			ListCoin [i].transform.position = _transform.position;
			ListCoin [i].SpawmCoin ();
		}
	}

	void ParticleDeath ()
	{
		if (particleDeath.isPlaying) {
			particleDeath.Stop ();
		}
		particleDeath.transform.position = transform.position;
		particleDeath.Play ();
	}
}
