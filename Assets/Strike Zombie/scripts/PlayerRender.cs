﻿using UnityEngine;
using System.Collections;

public class PlayerRender : MonoBehaviour {
	public DataGame dataGame;
	public PlayerControl player;
	public GameObject playerShop;
	public CameraMove cam;
	public UIManager ui;
	public ListEnemy listEnemy;
	public GameObject listCoin;
	public ParticleSystem particleDeath;
	public ParticleSystem particleMiss;
	Vector3 tranStart;

	void Awake()
	{
		tranStart = player.transform.position;
		SetSpritePlayer (dataGame.playerActive);
	}
	public void SetSpriteShop(int index)
	{
		GameObject playShop = (GameObject)Instantiate(dataGame.spritePlayer[index].playerShop, playerShop.transform.position, playerShop.transform.rotation);
		playShop.transform.SetParent (playerShop.transform.parent);
		Destroy (playerShop);
		playerShop = playShop;
	}

	public void SetSpritePlayer(int index)
	{
		GameObject playGame = (GameObject)Instantiate(dataGame.spritePlayer[index].player.gameObject, player.transform.position, player.transform.rotation);
		Destroy (player.gameObject);
		player = playGame.GetComponent <PlayerControl>();
		player.transform.position = tranStart;
		player.tranStart = tranStart;
		player.cameraMove = cam;
		player.listEnemy = listEnemy;
		player.particleDeath = particleDeath;
		player.uiManager = ui;
		ArrowControl arrow = player.archeryControl.arrowControl;
		arrow.managerUI = ui;
		arrow.listCoin = listCoin;
		arrow.listEnemy = listEnemy;
		arrow.particleDeath = particleDeath;
		arrow.particleMiss = particleMiss;
		arrow.SetStart ();
		cam.player = player.gameObject;
		ui.playerControl = player;
	}
}
