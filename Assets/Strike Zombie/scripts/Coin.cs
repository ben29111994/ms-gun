﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour
{
	public DataGame dataGame;
	public float addForce = 10;
	Rigidbody2D rigid;
	SpriteRenderer render;
	Animation anima;
	AudioSource audio;
	UIManager uiManager;
	// Use this for initialization
	void Awake ()
	{
		uiManager = GameObject.FindGameObjectWithTag ("UI").GetComponent<UIManager> ();
		rigid = GetComponent<Rigidbody2D> ();
		render = GetComponent<SpriteRenderer> ();
		anima = GetComponent<Animation> ();
		audio = GetComponent<AudioSource> ();
		audio.clip = dataGame.audioData.coin;
	}

	public void SpawmCoin ()
	{
		render.enabled = true;
		rigid.isKinematic = false;
		rigid.velocity = Vector2.zero;
		rigid.AddForce (new Vector2 (Random.Range (-addForce, addForce), Random.Range (-addForce, addForce)));
		render.color = new Vector4 (1, 1, 1, 1);
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.tag == "Ground") {
			anima.Play ();
			audio.Play ();
			uiManager.AddCoin (1);
			Invoke ("StopCoin", 1f);
		}
	}

	void StopCoin()
	{
		rigid.velocity = Vector2.zero;
		rigid.isKinematic = true;
		render.enabled = false;
	}
}
