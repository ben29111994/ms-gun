﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;

[System.Serializable]
public class RandomEnemy
{
	public OcclusionArea level;
	public int score;
	[HideInInspector]
	public Vector2 randomX;
	[HideInInspector]
	public Vector2 randomY;
}

public class EnemyControl : MonoBehaviour
{
	public RandomEnemy[] randomEnemy;
	public PlayerRender playerRender;
	PlayerControl playerControl
	{
		get{return playerRender.player;}
	}
	public SpearControl spearControl;
	public Vector2 addFor;
	public float deltaRestartPlayer = 10;

	Animator anima;
	public Rigidbody2D rigid;
	Vector3 tranEnemy;
	Vector3 angleEnemy;

	void Awake ()
	{
		anima = GetComponent<Animator> ();
		foreach (RandomEnemy enemy in randomEnemy) {
			enemy.randomX = new Vector2 (-playerControl.transform.position.x + enemy.level.transform.position.x + enemy.level.center.x - enemy.level.size.x / 2, -playerControl.transform.position.x + enemy.level.transform.position.x + enemy.level.center.x + enemy.level.size.x / 2);
			enemy.randomY = new Vector2 (enemy.level.transform.position.y + enemy.level.center.y - enemy.level.size.y / 2, enemy.level.transform.position.y + enemy.level.center.y + enemy.level.size.y / 2);
		}
		tranEnemy = rigid.transform.localPosition;
		angleEnemy = rigid.transform.localEulerAngles;
	}

	void Update ()
	{
		if (playerControl.transform.position.x - transform.position.x > deltaRestartPlayer) {
			SetEnemy ();
			gameObject.SetActive (false);
		}
	}

	public void SetEnemy ()
	{
		rigid.transform.localPosition = tranEnemy;
		rigid.transform.localEulerAngles = angleEnemy;
		spearControl.SetSpear ();
		rigid.isKinematic = true;
		anima.enabled = true;
		anima.SetBool ("death", false);
		anima.SetBool ("attack", false);
	}

	public void SetRandomEnemy ()
	{
		SetEnemy ();
		for (int i = randomEnemy.Length - 1; i >= 0; i--) {
			if (playerControl.dataGame.score >= randomEnemy [i].score) {
				transform.position = new Vector3 (playerControl.positionEnd.x + Random.Range (randomEnemy [i].randomX.x, randomEnemy [i].randomX.y),
					Random.Range (randomEnemy [i].randomY.x, randomEnemy [i].randomY.y),
					transform.position.z);
				return;
			}
		}
	}

	public void DieEnemy ()
	{
		anima.SetBool ("death", true);
		rigid.isKinematic = false;
		addFor = new Vector2 (Random.Range (100, 200), Random.Range (400, 800));
		rigid.AddForce (addFor);
	}

	public void AtTack ()
	{
		spearControl.Shoot ();
		anima.SetBool ("attack", true);
	}

}
