﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;


public class UIManager : MonoBehaviour
{
	//public GoogleMobileAdsDemoScript admob;
	public DataGame dataGame;
	public EventSystem eventSystem;
	public GameObject shopCam;
	public ShopManager shop;
	public GameObject pause;
	public GameObject play;
	public GameObject rate;
	public GameObject buttonShop;
	public GameObject restart;
	public GameObject sound;
	public GameObject sound1;
	public GameObject soundMuti;
	public GameObject soundMuti1;
	public GameObject buttonLeaderBoard;
	public GameObject gameOver;
	public GameObject shareLink;
	public GameObject shareScreen;
	public GameObject banelQuit;
	public Text best;
	public Text diem;
	public Text score1;
	public Text coin1;
	public Text best1;
	public Animator animatorShot;
	public ReviveGame revive;
	public GameObject moreGame;


	[HideInInspector]
	public Animator anima;
	public ListEnemy listEnemy;
	public PlayerControl playerControl;
	CameraMove cameraMove;
	Tips tip;
	bool quitTime;

    private bool isRevive = false;

	public void AddCoin (int add)
	{
		dataGame.coin += add;
		coin1.text = dataGame.coin.ToString ();
	}

	public void AddScore (int add)
	{
		dataGame.score += add;
		diem.text = dataGame.score.ToString ();
	}

	void Awake ()
	{
		revive.NoRevive.AddListener (ShowGameOver);
		revive.ReviveCoin.AddListener (ReviveCoin);
		revive.ReviveVideo.AddListener (ReviveVideo);
		if(dataGame.deleteAllData)
		{
			PlayerPrefs.DeleteAll ();
		}
		shopCam.SetActive (false);
		anima = GetComponent<Animator> ();
		coin1.text = dataGame.coin.ToString ();
		cameraMove = Camera.main.gameObject.GetComponent<CameraMove> ();
		tip = GetComponent<Tips> ();

	}

	// Use this for initialization
	void Start ()
	{
		CheckSound ();
		anima.SetBool ("showStart", true);
		dataGame.score = 0;
		diem.text = dataGame.score.ToString ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (Application.platform == RuntimePlatform.Android) {
			if (Input.GetKeyDown (KeyCode.Escape)) {
				if(play.activeSelf || restart.activeSelf)
				{
					Application.Quit ();
				}
				else
				{
					Application.LoadLevel (0);
				}
			}
		}
	}


	void ReviveCoin()
	{
		dataGame.coin -= revive.numberCoin;
		coin1.text = dataGame.coin.ToString ();
		ResumeGame ();
	}

	void ReviveVideo()
	{
        //		UnityAdsExample.Instance.ShowRewardedAd ();
        //		UnityAdsExample.Instance.Finish.AddListener (ResumeGame);

        UnityAdsManager.Instance.ForceShowAds();
        UnityAdsManager.Instance.AddCallback(ResumeGame);

    }

    void ResumeGame()
	{
		playerControl.SaveState ();
        isRevive = true;
    }

    public bool CheckRevive()
	{
        if (isRevive)
            return false;

//		GoogleMobileAdsDemoScript.instance.ShowInterstitial ();
		if(dataGame.score <= 0)
		{
			revive.gameObject.SetActive (false);
			return false;
		}
        
        bool coinRevive = dataGame.coin >= revive.numberCoin;
        bool videoRevive = UnityAdsManager.Instance.IsAdsReady();
        
        if(coinRevive && videoRevive)
        {
            revive.gameObject.SetActive(true);
            revive.SetRevive(ReviveType.Coin_Video, revive.numberCoin);
            return true;
        }

//		if(dataGame.coin >= revive.numberCoin && (UnityAdsExample.Instance.IsReady () && Random.Range (0,100)< revive.randomVideo))
//		{
//			revive.gameObject.SetActive (true);
//			revive.SetRevive (ReviveType.Coin_Video, revive.numberCoin);
//			return true;
//		}
//		else 
        if (coinRevive)
		{
			revive.gameObject.SetActive (true);
			revive.SetRevive (ReviveType.Coin, revive.numberCoin);
			return true;
		}
//		else if((UnityAdsExample.Instance.IsReady () && Random.Range (0,100)< revive.randomVideo))
//		{
//			revive.gameObject.SetActive (true);
//			revive.SetRevive (ReviveType.Video);
//			return true;
//		}

        if(videoRevive)
        {
            revive.gameObject.SetActive(true);
            revive.SetRevive(ReviveType.Video);
            return true;
        }

        revive.gameObject.SetActive (false);
		return false;
	}


	public void EventPlay ()
	{
		eventSystem.firstSelectedGameObject = play;
	}

	public void ButtonOK ()
	{
		Application.Quit ();
	}

	public void ButtonShop ()
	{
		anima.SetBool ("showShop", true);
	}

	public void PingCoin ()
	{
		anima.SetTrigger ("ping");
	}

	public void ShowShop ()
	{
		shopCam.SetActive (true);
	}

	public void HideShop ()
	{
		shopCam.SetActive (false);
	}

	public void ButtonHideShop ()
	{
		anima.SetBool ("showShop", false);
	}

	public void ButtonExit ()
	{
		banelQuit.SetActive (false);
		if (quitTime)
			Time.timeScale = 1;
		else
			Time.timeScale = 0;
	}

	public void GameOver ()
	{
		if(!CheckRevive ())
		{
			ShowGameOver ();
		}

	}

	void ShowGameOver()
	{
//		GoogleMobileAdsDemoScript.instance.HideBanner ();
		anima.SetBool ("showGameOver", true);
		if (PlayerPrefs.GetInt ("hightScore") < dataGame.score) {
			PlayerPrefs.SetInt ("hightScore", dataGame.score);      
		}
		best.text = "Best" + PlayerPrefs.GetInt ("hightScore").ToString ();
		best1.text = PlayerPrefs.GetInt ("hightScore").ToString ();
		score1.text = diem.text;
		tip.DestroyTips ();
        //		Service.instance.ReportScore (dataGame.score);
        UnityAdsManager.Instance.ShowAds();
        isRevive = false;
    }


    public void BottonRestart ()
	{
//		GoogleMobileAdsDemoScript.instance.ShowBanner ();
		anima.SetBool ("showGameOver", false);
		dataGame.score = 0;
		diem.text = dataGame.score.ToString ();
		listEnemy.ResetEnemy ();
		playerControl.SetPlayer ();
		cameraMove.SetCamera ();
		tip.TipRandom ();
		Invoke ("DelayDestroyTip", 2f);
		revive.numberCoin = revive.coinRevive;

        SoundManager.Instance.RandomSound();
	}

	public void ReviveGame()
	{
//		GoogleMobileAdsDemoScript.instance.ShowBanner ();
		playerControl.SaveState ();
	}

	public void BottonRate ()
	{
		#if UNITY_ANDROID
		Application.OpenURL(AppInfo.Instance.PLAYSTORE_LINK);
		#elif UNITY_IOS
		Application.OpenURL(AppInfo.Instance.APPSTORE_LINK);
		#endif
	}

	public void ButtonPlay ()
	{
//		moreGame.SetActive(false);
//		GoogleMobileAdsDemoScript.instance.ShowBanner ();
		
		revive.numberCoin = revive.coinRevive;
		Time.timeScale = 1;
		if (playerControl.setPlay) { // PLAY LAN DAU
			anima.SetBool ("showStart", false);
			tip.StartGame ();
			playerControl.setPlay = false;
			playerControl.SetRun ();
			Invoke ("DelayDestroyTip", 2f);   
		}
		anima.SetBool ("showPause", false);
		anima.SetBool ("play", true);
        SoundManager.Instance.RandomSound();
    }

    public void BottonPause ()
	{
		anima.SetBool ("showPause", true);
	}

	public void TimeScale (int scale)
	{
		Time.timeScale = scale;
	}

	public void BottonSound ()
	{
		AudioListener.pause = true;
		soundMuti.SetActive (true);
		sound.SetActive (false);
		soundMuti1.SetActive (true);
		sound1.SetActive (false);
		dataGame.volum = 0;
	}

	public void BottonSoundMuti ()
	{
		soundMuti.SetActive (false);
		sound.SetActive (true);
		soundMuti1.SetActive (false);
		sound1.SetActive (true);
		dataGame.volum = 1;
		AudioListener.pause = false;
	}

	void DelayDestroyTip ()
	{
		tip.DestroyTips ();
	}

	public void Shot (string text, int adScore)
	{
		animatorShot.gameObject.transform.GetChild (0).gameObject.GetComponent<Text> ().text = text;
		animatorShot.SetTrigger ("shot");
		AddScore (adScore);
	}

	public void CheckSound ()
	{
		if (dataGame.volum != 0) {
			BottonSoundMuti ();
		} else {
			BottonSound ();
		}
	}

    public void ButtonFacebook()
    {

    }

    public void ButtonRate()
    {

    }

}