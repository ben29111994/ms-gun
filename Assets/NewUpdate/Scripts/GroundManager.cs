﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundManager : MonoBehaviour {
	public GameObject[] groundTypePrefab;
	public float endPos;
	public float distance;
	float targetPosY;
	// Use this for initialization
	void Start () {
		targetPosY = distance;
	}
	
	// Update is called once per frame
	void Update () {
		SpawnGround ();
	}
	void SpawnGround(){
		if (PlayerControl.instance.transform.position.y - transform.position.y > targetPosY) {
			targetPosY += distance;
			endPos += distance;

			GameObject groundClone = Instantiate (groundTypePrefab [Random.Range (0, groundTypePrefab.Length - 1)]) as GameObject;
			groundClone.transform.localPosition = new Vector2(groundClone.transform.position.x ,  endPos);
			groundClone.transform.SetParent (transform);
		}
	}
}
