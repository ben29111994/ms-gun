﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundD : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector2.Distance (PlayerControl.instance.transform.position, transform.position);
		if (distance > 100) {
			Destroy (gameObject);
		}
	}
}
