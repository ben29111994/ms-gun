﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoSingleton<SoundManager>
{

    private AudioSource source;
    public List<AudioClip> sounds;

    public void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    public void RandomSound()
    {
        var clip = sounds[Random.Range(0, sounds.Count)];

        if (clip != null)
        {
            source.Stop();
            source.clip = clip;
            source.Play();
        }
    }
}
