using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;

public class UnityAdsManager : MonoSingleton<UnityAdsManager>
{
    private string GooglePlayGameID = "1557797";
    private string AppStoreGameID = "1557796";
    private string gameID;

    private float timeLastAdsShow = 0;
    public float timeBetweenAds = 120;
    private bool firstTime = true;

    private UnityEvent resultCallback;

    // Use this for initialization
    void Awake()
    {
#if UNITY_ANDROID
        gameID = GooglePlayGameID;
#elif UNITY_IOS
		gameID = AppStoreGameID;
#endif
        Advertisement.Initialize(gameID);
        resultCallback = new UnityEvent();
    }

    private bool CanShowAds()
    {
        if (firstTime)
        {
            firstTime = false;
            timeLastAdsShow = Time.realtimeSinceStartup;
            return true;
        }
        else
        {
            float currentTime = Time.realtimeSinceStartup;

            if ( currentTime - timeLastAdsShow >= timeBetweenAds)
            {
                timeLastAdsShow = currentTime;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public void ShowAds(string placementID = "")
    {
        StartCoroutine(WaitForAd());
        
        if (string.Equals (placementID, ""))
            placementID = null;
        
        ShowOptions options = new ShowOptions ();
        options.resultCallback = AdCallbackhandler;

        if (Advertisement.IsReady(placementID) && CanShowAds())
            Advertisement.Show(placementID);
        
    }

    public bool IsAdsReady(string placementID = "")
    {
        return Advertisement.IsReady(placementID);
    }


    public void ForceShowAds(string placementID = "")
    {
        StartCoroutine(WaitForAd());

        if (string.Equals(placementID, ""))
            placementID = null;

        ShowOptions options = new ShowOptions();
        options.resultCallback = AdCallbackhandler;
        timeLastAdsShow = Time.realtimeSinceStartup;

        Advertisement.Show(placementID, options);
    }


    void AdCallbackhandler (ShowResult result)
    {
        switch(result)
        {
            case ShowResult.Finished:
                Debug.Log ("Ad Finished. Rewarding player...");
                break;
            case ShowResult.Skipped:
                Debug.Log ("Ad skipped. Son, I am dissapointed in you");
                break;
            case ShowResult.Failed:
                Debug.Log("I swear this has never happened to me before");
                break;
        }

        resultCallback.Invoke();
        resultCallback.RemoveAllListeners();
    }
    
    IEnumerator WaitForAd()
    {
        float currentTimeScale = Time.timeScale;
        Time.timeScale = 0f;
        yield return null;

        while (Advertisement.isShowing)
            yield return null;

        Time.timeScale = currentTimeScale;
    }

    public void AddCallback(UnityAction action)
    {
        resultCallback.AddListener(action);
    }
}