﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [Header("Object References")]
    [SerializeField]
    private GameObject startGameUI;
    [SerializeField]
    private GameObject inGameUI;
    [SerializeField]
    private GameObject endGameUI;
    [SerializeField]
    private GameObject buttons;

    [SerializeField]
    private Button btnPlay;
    [SerializeField]
    private Button btnRestart;
    [SerializeField]
    private Button btnShop;
    [SerializeField]
    private Button btnRate;
    [SerializeField]
    private Button btnFacebook;

    [SerializeField]
    private Text txtScore;
    [SerializeField]
    private Text txtHighScore;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public void ShowStartGameUI()
    {
        startGameUI.SetActive(true);
        buttons.SetActive(true);
        btnPlay.gameObject.SetActive(true);
    }

    public void HideStartGameUI()
    {
        startGameUI.SetActive(false);
        buttons.SetActive(false);
        btnPlay.gameObject.SetActive(false);
    }

    public void ShowInGameUI()
    {
        inGameUI.SetActive(true);
    }

    public void HideInGameUI()
    {
        inGameUI.SetActive(false);
    }

    public void ShowEndGameUI()
    {
        endGameUI.SetActive(true);
        buttons.SetActive(true);
        btnRestart.gameObject.SetActive(true);
    }

    public void HideEndGameUI()
    {
        endGameUI.SetActive(false);
        buttons.SetActive(false);
        btnRestart.gameObject.SetActive(false);
    }

}
